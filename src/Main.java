// - There is a line of comment


// class - is a template/blueprint for objects(instance)
class Main {

    PONUATNO
    KIRILL CHTO_TO POSHLO NE TAK!
    // method/function ->
    // public -> we can read it from any place in the program
    // static -> is a property of Main
    // void -> method won't return anything

    //   This signature - point from where will start our program
    public static void main(String[] args) {

        // I came here to become the greatest programmer in the world!
        System.out.println("I'm the future of programming!");
//        NEW CHANGEs
        //    Code of your program should be here

        //        System - is a package of JDK which allows us to call the system
        //        .out- method of System

        //        initialisation
        //        declaration

        //        ; - dot with coma is the end of operation of init/declar, method call/execution

        System.out.println("I came here to make you great Developer!");

        byte priceOfCandy = 32;

        int favouriteNumber = 23; // [23]
        short priceOfSmartphoneXiaome = 126;
        int vertuPrice = 1231235126;
        long priceOfFabric = 1231226834;

        boolean isAdmin = false;
        boolean hasLimit = false;
        boolean hasBroken = false;
//
//        if(hasBroken) {
//            System.out.println("I wanna buy new one!");
//        }

        System.out.println(favouriteNumber); // [23]
        favouriteNumber += 13; // 23 + 13 => 36
        int davidsFavouriteNumber = 9;

        int commonFavouriteNumber = favouriteNumber + davidsFavouriteNumber;


        String name = "David";

        System.out.println(commonFavouriteNumber);
        System.out.println("Hello");
        System.out.println("my friend");

        System.out.println("The end of a program");

        System.out.println(favouriteNumber); // 36
    }
}

